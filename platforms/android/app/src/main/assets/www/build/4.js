webpackJsonp([4],{

/***/ 601:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SlotbookingPageModule", function() { return SlotbookingPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__slotbooking__ = __webpack_require__(620);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SlotbookingPageModule = (function () {
    function SlotbookingPageModule() {
    }
    SlotbookingPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__slotbooking__["a" /* SlotbookingPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__slotbooking__["a" /* SlotbookingPage */]),
            ],
        })
    ], SlotbookingPageModule);
    return SlotbookingPageModule;
}());

//# sourceMappingURL=slotbooking.module.js.map

/***/ }),

/***/ 620:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SlotbookingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_provider_provider__ = __webpack_require__(355);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the SlotbookingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SlotbookingPage = (function () {
    function SlotbookingPage(navCtrl, navParams, modalCtrl, db, alert) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.db = db;
        this.alert = alert;
        this.Data = {
            doctor: '',
            date: ''
        };
        this.query2 = [this.Data];
        this.doctors = this.navParams.get("selectdoctors");
        this.user = this.navParams.get("userEmail");
    }
    SlotbookingPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SlotbookingPage');
    };
    SlotbookingPage.prototype.booknow = function () {
        this.navCtrl.push('MorningslotPage', { 'details': this.Data, "userEmail": this.user });
        //console.log(this.query2);
    };
    SlotbookingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-slotbooking',template:/*ion-inline-start:"/home/krishna/krishna/projects/Dr.Application/src/pages/slotbooking/slotbooking.html"*/'<ion-header>\n    <ion-navbar color="bluegreen">\n       \n          <!-- <img class="grid-image" src="./assets/icon/doct.png"> -->\n          <ion-title>Doctor Appointment</ion-title>\n        \n       \n      </ion-navbar>\n\n \n  </ion-header>\n\n      <!-- Doctor Appointment -->\n  <!-- <ion-row>\n\n    <ion-col col-2>\n      <div class="profile-left">\n        <img class="grid-image" src="./assets/icon/doct.png">\n      </div>\n\n\n    </ion-col>\n    <ion-col col-7>\n      <div class="name-right">\n       Doctor Appointment\n      </div>\n\n    </ion-col>\n    <ion-col col-2>\n      <button ion-button class="hlines" menuToggle right>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-col>\n\n\n\n  </ion-row> -->\n\n\n\n<ion-content padding>\n<form #detailsFillingForm="ngForm">\n<ion-item  >\n  \n  <ion-label floating color="bluegreen">Doctor</ion-label>\n\n  <ion-select required  *ngIf="doctors == \'Cardiologist\'" interface="popover" name="doctor" [(ngModel)]="Data.doctor"   >\n    <ion-option value="suresh@cardiologist.com">Dr.Suresh Kumar</ion-option>\n    <ion-option value="harsha@cardiologist.com">Dr.Harsha Vardhan</ion-option>\n    <!-- <div *ngIf="DrSelect.invalid && (DrSelect.dirty || DrSelect.touched)" style="color:red;">\n\n      <div *ngIf="DrSelect.errors.required">\n          Field is required.\n      </div>\n  </div> -->\n  </ion-select>\n  \n\n  <ion-select required *ngIf="doctors == \'Nephrologist\'" interface="popover" name="doctor" [(ngModel)]="Data.doctor"   >\n      <ion-option value="venkateswarao@Nephrology.com">Dr.Venkateswarao</ion-option>\n      <ion-option value="ananth@Nephrology.com">Dr.Ananth</ion-option>\n    </ion-select>\n\n    <ion-select required *ngIf="doctors == \'Pulmonologist\'" interface="popover" name="doctor" [(ngModel)]="Data.doctor"   >\n        <ion-option value="gopalrao@Pulmonology.com">Dr.Gopalrao Rao</ion-option>\n      </ion-select>\n\n      <ion-select required *ngIf="doctors == \'Gastroenterologist\'" interface="popover" name="doctor" [(ngModel)]="Data.doctor"   >\n          <ion-option value="srinivas@Gastroenterology.com">Dr.Srinivas</ion-option>\n        </ion-select>\n\n        <ion-select required *ngIf="doctors == \'Neurologist\'" interface="popover" name="doctor" [(ngModel)]="Data.doctor"   >\n            <ion-option value="sameer@neuro.com">Dr.Sameer</ion-option>\n          </ion-select>\n\n</ion-item>\n\n<ion-item>\n  <ion-label  floating color="light">Select Date</ion-label>\n  <ion-input required type="date" name="date" [(ngModel)]="Data.date"></ion-input>\n</ion-item>\n\n\n<ion-row>\n  <ion-col>\n   \n    <button ion-button round class="Book" (click)="booknow()" [disabled]="detailsFillingForm.invalid">Booking Slot</button>\n  </ion-col>\n  \n</ion-row>\n\n\n</form>\n\n\n\n</ion-content>'/*ion-inline-end:"/home/krishna/krishna/projects/Dr.Application/src/pages/slotbooking/slotbooking.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ModalController */], __WEBPACK_IMPORTED_MODULE_2__providers_provider_provider__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], SlotbookingPage);
    return SlotbookingPage;
}());

//# sourceMappingURL=slotbooking.js.map

/***/ })

});
//# sourceMappingURL=4.js.map