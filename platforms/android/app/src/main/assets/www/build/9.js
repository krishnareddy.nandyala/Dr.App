webpackJsonp([9],{

/***/ 593:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home__ = __webpack_require__(612);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var HomePageModule = (function () {
    function HomePageModule() {
    }
    HomePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__home__["a" /* HomePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__home__["a" /* HomePage */]),
            ],
        })
    ], HomePageModule);
    return HomePageModule;
}());

//# sourceMappingURL=home.module.js.map

/***/ }),

/***/ 612:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(182);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HomePage = (function () {
    function HomePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.user = this.navParams.get("userEmail");
    }
    HomePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad HomePage');
    };
    HomePage.prototype.AboutUs = function () {
        this.navCtrl.push('HosptaldetailsPage');
    };
    HomePage.prototype.specialist = function () {
        this.navCtrl.push('SpecialistPage', { 'userEmail': this.user });
    };
    HomePage.prototype.cancelBooking = function () {
        this.navCtrl.push('CancelbookingPage', { 'userEmail': this.user });
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-home',template:/*ion-inline-start:"/home/krishna/krishna/projects/Dr.Application/src/pages/home/home.html"*/'<ion-header>\n\n    <ion-navbar color="bluegreen">\n        <ion-row>\n          <img class="grid-image" src="./assets/icon/doct.png">\n          <ion-title>Welcome</ion-title>\n        </ion-row>\n        <button ion-button menuToggle right>\n            <ion-icon name="menu"></ion-icon>\n          </button>\n      </ion-navbar>\n\n  <!-- <ion-row>\n    \n\n    <ion-col col-2>\n      <div class="profile-left">\n        <img class="grid-image" src="./assets/icon/doct.png">\n      </div>\n\n\n    </ion-col>\n    <ion-col col-7>\n      <div class="name-right">\n        Welcome\n      </div>\n\n    </ion-col>\n    <ion-col col-2>\n      \n    </ion-col>\n\n  </ion-row> -->\n</ion-header>\n\n<ion-content padding class="auth-page" >\n  <!-- <h6 color="bluegreen">Appointment Fee:Rs400(1st Consultation)</h6> -->\n\n  <ion-row>\n      <ion-col col-1>\n        </ion-col>  \n    <ion-col col-5 class="dash-icons" style="margin-right: 10px;">\n          <img class="Allfecilities" src="./assets/icon/stethoscope.png" (click)="specialist()">\n          <p class="text">Specialist</p>\n        </ion-col>\n     \n    <ion-col col-5 class="dash-icons">\n        <ion-icon class="Allfecilities" (click)="AboutUs()" name="contact"></ion-icon>\n      <p class="text">About us</p>\n    </ion-col>\n    <ion-col col-1>\n      </ion-col>\n  </ion-row>\n\n  <ion-row>\n      <ion-col col-1>\n        </ion-col>  \n    <ion-col col-5 class="dash-icons" style="margin-right: 10px;">\n        <ion-icon class="Allfecilities" ios="ios-chatboxes" md="md-chatboxes"></ion-icon>\n      <p class="text">Chat</p>\n    </ion-col>\n     \n    <ion-col col-5 class="dash-icons">\n        <ion-icon class="Allfecilities" name="call"></ion-icon>\n      <p >Call</p>\n    </ion-col>\n    <ion-col col-1>\n      </ion-col>\n  </ion-row>\n  <ion-row>\n      <ion-col col-1>\n        </ion-col>  \n    <ion-col col-5 class="dash-icons" style="margin-right: 10px;" (click)="cancelBooking()">\n      <img class="Allfecilities" src="./assets/imgs/cancellingbokking.png">\n      <p>Cancel Booking</p>\n    </ion-col>\n  \n    \n    <ion-col col-5 class="dash-icons">\n\n        <ion-icon class="Allfecilities" name="paper"></ion-icon>\n      <p class="text">Feed Back</p>\n    </ion-col>\n    <ion-col col-1>\n      </ion-col>\n  </ion-row>\n\n  <!-- <button class="ApointmentButton" ion-button round block navPush="BookPage">Book an Appointment</button>\n  <br>\n  <h6 color="bluegreen">Locations</h6>\n\n  <ion-card>\n    <ion-row>\n      <ion-col col-2>\n        <img class="NavImg" src="assets/imgs/maps.png">\n      </ion-col>\n      <ion-col col-8>\n        <p>Omega clinic,Bhagyanagar</p>\n        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Time 9:00am-1:00pm</p>\n      </ion-col>\n\n\n    </ion-row>\n    <hr>\n    <ion-row>\n      <ion-col col-2>\n        <img class="NavImg" src="assets/imgs/maps.png">\n      </ion-col>\n      <ion-col col-9>\n        <p>Omega clinic,Addagutta Socity</p>\n        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Time 5:00pm-8:00pm</p>\n      </ion-col>\n\n\n\n    </ion-row>\n  </ion-card> -->\n\n</ion-content>\n\n<ion-footer>\n  <ion-row>\n\n  </ion-row>\n</ion-footer>'/*ion-inline-end:"/home/krishna/krishna/projects/Dr.Application/src/pages/home/home.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ })

});
//# sourceMappingURL=9.js.map