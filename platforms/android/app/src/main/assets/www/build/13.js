webpackJsonp([13],{

/***/ 589:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DrLoginPageModule", function() { return DrLoginPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__dr_login__ = __webpack_require__(608);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var DrLoginPageModule = (function () {
    function DrLoginPageModule() {
    }
    DrLoginPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__dr_login__["a" /* DrLoginPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__dr_login__["a" /* DrLoginPage */]),
            ],
        })
    ], DrLoginPageModule);
    return DrLoginPageModule;
}());

//# sourceMappingURL=dr-login.module.js.map

/***/ }),

/***/ 608:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DrLoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(182);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the DrLoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DrLoginPage = (function () {
    function DrLoginPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    DrLoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DrLoginPage');
    };
    DrLoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-dr-login',template:/*ion-inline-start:"/home/krishna/krishna/projects/Dr.Application/src/pages/dr-login/dr-login.html"*/'<ion-header>\n    \n  <ion-row>\n\n    <ion-col col-2>\n      <div class="profile-left">\n        <img class="grid-image" src="./assets/imgs/DoctorMan.png">\n      </div>\n    \n    \n    </ion-col>\n    <ion-col col-7>    \n    <div class="name-right">\n    <h4 class="DrName">Dr.Srikanth Kumar\n      <br>Dental Clinic Care</h4>\n  </div>\n\n</ion-col>\n<ion-col col-2>\n  <button ion-button menuToggle right>\n      <ion-icon name="menu" class="hlines"></ion-icon>\n    </button>\n</ion-col>\n\n\n\n  </ion-row>\n</ion-header>\n<ion-content padding>\n  \n\n  <ion-row>\n    <ion-col col-4>\n      <img class="Allfecilities" style="left: 23px;" navPush="DrprofileDetailsPage" src="./assets/imgs/aboutUs.png">\n      <p class="text">About us</p>\n    </ion-col>\n    <ion-col col-4>\n      <img class="Allfecilities" src="./assets/imgs/services.jpg">\n      <p class="text">Services</p>\n    </ion-col>\n    <ion-col col-4>\n      <img class="Allfecilities" src="./assets/imgs/Chat.png">\n      <p class="text">Chat</p>\n    </ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col col-4>\n      <img class="Allfecilities" src="./assets/imgs/Prescription.jpg">\n      <p>Dr.priscription</p>\n    </ion-col>\n    <ion-col col-4>\n      <img class="Allfecilities" src="./assets/imgs/call.png">\n      <p class="text">Call</p>\n    </ion-col>\n    <ion-col col-4>\n      <img class="Allfecilities" src="./assets/imgs/availabilityimg.png">\n      <p class="text">Availibility</p>\n    </ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col col-4>\n      <img class="Allfecilities" src="./assets/imgs/Location.png">\n      <p class="text">Location</p>\n    </ion-col>\n    <ion-col col-4>\n      <img class="Allfecilities" src="./assets/imgs/Reportt.png">\n      <p class="text">Reports</p>\n    </ion-col>\n    <ion-col col-4>\n      <img class="Allfecilities" src="./assets/imgs/feedback.png">\n      <p class="text">Feed Back</p>\n    </ion-col>\n  </ion-row>\n<br>\n<br>    \n <br>\n  <button color="light" class="allappoinbtn" ion-button block round navPush="AppointmentListPage">All Appointments\n\n  </button>\n\n</ion-content>\n\n<ion-footer class="FooterHei">\n  <ion-row>\n   \n  </ion-row>\n</ion-footer>\n'/*ion-inline-end:"/home/krishna/krishna/projects/Dr.Application/src/pages/dr-login/dr-login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], DrLoginPage);
    return DrLoginPage;
}());

//# sourceMappingURL=dr-login.js.map

/***/ })

});
//# sourceMappingURL=13.js.map