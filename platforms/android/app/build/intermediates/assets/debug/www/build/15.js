webpackJsonp([15],{

/***/ 587:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CancelbookingPageModule", function() { return CancelbookingPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__cancelbooking__ = __webpack_require__(606);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var CancelbookingPageModule = (function () {
    function CancelbookingPageModule() {
    }
    CancelbookingPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__cancelbooking__["a" /* CancelbookingPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__cancelbooking__["a" /* CancelbookingPage */]),
            ],
        })
    ], CancelbookingPageModule);
    return CancelbookingPageModule;
}());

//# sourceMappingURL=cancelbooking.module.js.map

/***/ }),

/***/ 606:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CancelbookingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_provider_provider__ = __webpack_require__(355);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CancelbookingPage = (function () {
    function CancelbookingPage(navCtrl, navParams, service) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.service = service;
        //data:FormGroup;
        this.edit = true;
        this.DoctorDetails = {
            langE: '',
            langH: '',
            LabgT: '',
            Ecperience: ''
        };
        this.bookingDetails = [];
        this.daywiseList = [];
        this.drprofile = true;
        // BookingInfromations: Observable<data[]>;
        this.BookingInfromations = {};
        this.Data1 = [];
        this.Data = {};
        this.booked = [];
        this.c = [];
        this.d = [];
        this.NamesOfusers = [];
        this.user = this.navParams.get('userEmail');
        this.BookingInfromations = this.service.
            getUserDetails().
            query.on('value', function (personSnapshot) {
            _this.selctedslotds = personSnapshot.val();
            console.log("bookingDetails", personSnapshot.val());
            var d = _this.selctedslotds;
            var a = [];
            _this.booked = [];
            for (var x in d) {
                a.push(x);
            }
            // console.log(a);
            for (var i = 0; i < a.length; i++) {
                var obj = { id: a[i] };
                var obj2 = {
                    date: d[a[i]][0].date,
                    doctor: d[a[i]][0].doctor,
                    patientname: d[a[i]][0].patientname,
                    useremail: d[a[i]][0].user,
                    time: d[a[i]][1],
                    obj: obj
                };
                _this.booked.push(obj2);
                console.log(" this.booked", _this.booked);
            }
            _this.bookingDetails = _this.booked;
            console.log("bookingDetails", _this.bookingDetails);
        });
        this.Data = this.service.getUserName().query.on('value', function (Username) {
            _this.Data1 = Username.val();
            // console.log(this.Data1); 
            _this.names = _this.Data1;
            // console.log(this.names)
            var y = [];
            for (var z in _this.names) {
                y.push(z);
            }
            for (var j = 0; j < y.length; j++) {
                var name = {
                    username: _this.names[y[j]]
                };
                _this.NamesOfusers.push(name);
            }
            _this.bookingDetails.forEach(function (x, index) {
                //x.username = this.NamesOfusers[index].username;
            });
            console.log(_this.bookingDetails);
        });
    }
    CancelbookingPage.prototype.Edit = function () {
        this.edit = true;
        this.abc = false;
    };
    CancelbookingPage.prototype.DrProfile = function () {
        this.drprofile = true;
        this.appointment = false;
    };
    CancelbookingPage.prototype.Appointment = function () {
        this.drprofile = false;
        this.appointment = true;
    };
    CancelbookingPage.prototype.showDetails = function () {
        var _this = this;
        this.patientsList = this.bookingDetails.filter(function (x) {
            if (x.useremail == _this.user) {
                return x;
            }
        });
    };
    CancelbookingPage.prototype.delete = function (key) {
        var _this = this;
        this.service.removeUser(key);
        // this.navCtrl.push('HomePage')
        this.patientsList = this.bookingDetails.filter(function (x) {
            if (x.useremail == _this.user) {
                return x;
            }
        });
    };
    CancelbookingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-cancelbooking',template:/*ion-inline-start:"/home/krishna/krishna/projects/Dr.Application/src/pages/cancelbooking/cancelbooking.html"*/'<ion-header>\n    <ion-navbar color="bluegreen">\n       \n          <!-- <img class="grid-image" src="./assets/icon/doct.png"> -->\n          <ion-title>Cancel Bookingt</ion-title>\n        \n       \n      </ion-navbar>\n\n \n  </ion-header>\n\n\n<ion-content padding class="auth-page">\n  <button ion-button class="ShowDetails" (click)="showDetails()">Show Appointments</button>\n    <ion-row>\n\n        <ion-col col-12>\n            <ion-card *ngFor="let it of patientsList; let i=index">\n        \n              <ion-card-content>\n                <p class="details">\n                  <strong>Name:&nbsp; {{it.patientname}}</strong>\n                </p>\n                <p class="details">\n                  <strong>Date:</strong> {{it.date }}</p>\n                <p class="details">\n                  <strong>Time:</strong> {{it.time.time}}</p>\n        <button ion-button class="ShowDetails" (click)="delete(it.obj.id)">Delete</button>\n              </ion-card-content>\n            </ion-card>\n          </ion-col>\n        \n          </ion-row>\n\n</ion-content>\n'/*ion-inline-end:"/home/krishna/krishna/projects/Dr.Application/src/pages/cancelbooking/cancelbooking.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_provider_provider__["a" /* AuthProvider */]])
    ], CancelbookingPage);
    return CancelbookingPage;
}());

//# sourceMappingURL=cancelbooking.js.map

/***/ })

});
//# sourceMappingURL=15.js.map