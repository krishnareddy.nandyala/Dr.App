webpackJsonp([6],{

/***/ 597:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PatientDrprofilePageModule", function() { return PatientDrprofilePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__patient_drprofile__ = __webpack_require__(616);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PatientDrprofilePageModule = (function () {
    function PatientDrprofilePageModule() {
    }
    PatientDrprofilePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__patient_drprofile__["a" /* PatientDrprofilePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__patient_drprofile__["a" /* PatientDrprofilePage */]),
            ],
        })
    ], PatientDrprofilePageModule);
    return PatientDrprofilePageModule;
}());

//# sourceMappingURL=patient-drprofile.module.js.map

/***/ }),

/***/ 616:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PatientDrprofilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_provider_provider__ = __webpack_require__(355);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the PatientDrprofilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PatientDrprofilePage = (function () {
    function PatientDrprofilePage(navCtrl, provider, navParams) {
        // this.result=this.provider.getDrDetails().snapshotChanges().
        this.navCtrl = navCtrl;
        this.provider = provider;
        this.navParams = navParams;
        //   map(
        //     changes => {
        //       console.log( changes.map(c => ({key : c.payload.key, ...c.payload.val()})))
        //       return changes.map(c => ({
        //          key : c.payload.key, ...c.payload.val()
        //       }))
        //     })
        //     this.drdetailss=this.result;
    }
    PatientDrprofilePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PatientDrprofilePage');
    };
    PatientDrprofilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-patient-drprofile',template:/*ion-inline-start:"/home/krishna/krishna/projects/Dr.Application/src/pages/patient-drprofile/patient-drprofile.html"*/'<!--\n  Generated template for the PatientDrprofilePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="bluegreen">\n    <ion-title>Dr.profile</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<!-- <ion-content padding>\n  <ion-card class="cardclass">\n    \n     <span *ngFor="let Ab of drdetailss | async" \n     detail-push >\n     <ion-item><strong>HOSPTIAL:</strong>{{Ab.qualification}}</ion-item>\n     \n       <ion-item><strong>BRACH:</strong>{{Ab.experience}}</ion-item>\n   \n     <ion-item><strong>LOCATION:</strong>{{Ab.language}}</ion-item>\n    <br>\n    <br>\n  \n       \n       </span>\n   </ion-card>\n</ion-content> -->\n'/*ion-inline-end:"/home/krishna/krishna/projects/Dr.Application/src/pages/patient-drprofile/patient-drprofile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__providers_provider_provider__["a" /* AuthProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], PatientDrprofilePage);
    return PatientDrprofilePage;
}());

//# sourceMappingURL=patient-drprofile.js.map

/***/ })

});
//# sourceMappingURL=6.js.map