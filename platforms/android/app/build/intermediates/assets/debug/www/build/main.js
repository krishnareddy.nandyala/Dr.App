webpackJsonp([18],{

/***/ 196:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 196;

/***/ }),

/***/ 238:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/afternoon/afternoon.module": [
		585,
		17
	],
	"../pages/appointment-list/appointment-list.module": [
		586,
		16
	],
	"../pages/cancelbooking/cancelbooking.module": [
		587,
		15
	],
	"../pages/dr-gopalrao/dr-gopalrao.module": [
		588,
		14
	],
	"../pages/dr-login/dr-login.module": [
		589,
		13
	],
	"../pages/drprofile-details/drprofile-details.module": [
		590,
		12
	],
	"../pages/edit/edit.module": [
		591,
		11
	],
	"../pages/evening/evening.module": [
		592,
		10
	],
	"../pages/home/home.module": [
		593,
		9
	],
	"../pages/hosptaldetails/hosptaldetails.module": [
		594,
		8
	],
	"../pages/login/login.module": [
		595,
		2
	],
	"../pages/morningslot/morningslot.module": [
		596,
		7
	],
	"../pages/patient-drprofile/patient-drprofile.module": [
		597,
		6
	],
	"../pages/reset-password/reset-password.module": [
		598,
		1
	],
	"../pages/settings/settings.module": [
		599,
		5
	],
	"../pages/signup/signup.module": [
		600,
		0
	],
	"../pages/slotbooking/slotbooking.module": [
		601,
		4
	],
	"../pages/specialist/specialist.module": [
		602,
		3
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 238;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 345:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return config; });
var config = {
    apiKey: "AIzaSyDkxr_iaRXRWXF7wzLbWucOZ2S5DX5FBbA",
    authDomain: "draplication.firebaseapp.com",
    databaseURL: "https://draplication.firebaseio.com",
    projectId: "draplication",
    storageBucket: "",
    messagingSenderId: "441405301889"
};
//# sourceMappingURL=firebase.credentials.js.map

/***/ }),

/***/ 355:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_firebase__ = __webpack_require__(239);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(281);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var AuthProvider = (function () {
    function AuthProvider(database) {
        this.database = database;
        this.DrDetails = this.database.list('DrDetails');
        this.HospitalDetails = this.database.list('Hospotal');
        this.UserName = this.database.list('Username');
        this.UserAppointmentDetails = this.database.list('UserAppointmentDetails');
        this.details = {
            Hospotal: '',
            branch: '',
            location: '',
            key: ''
        };
    }
    AuthProvider.prototype.addHospitalDetails = function (details) {
        return this.HospitalDetails.push(details);
    };
    AuthProvider.prototype.getHospitalDetails = function () {
        return this.HospitalDetails;
    };
    AuthProvider.prototype.updateHospitalDetails = function (details) {
        return this.HospitalDetails.update(details.key, details);
    };
    // *************************************
    AuthProvider.prototype.removeUser = function (key) {
        this.UserAppointmentDetails.remove(key).then(function (_) { return console.log('item deleted!'); });
    };
    AuthProvider.prototype.addDrDetails = function (details) {
        return this.DrDetails.push(details);
    };
    AuthProvider.prototype.getDrDetails = function () {
        return this.DrDetails;
    };
    AuthProvider.prototype.updatedr = function (details) {
        return this.DrDetails.update(details.key, details);
    };
    AuthProvider.prototype.addUser = function (Data) {
        return this.UserAppointmentDetails.push(Data);
    };
    AuthProvider.prototype.getUserDetails = function () {
        console.log(this.UserAppointmentDetails);
        return this.UserAppointmentDetails;
    };
    AuthProvider.prototype.addUserName = function (patientname) {
        return this.UserName.push(patientname);
    };
    AuthProvider.prototype.getUserName = function () {
        return this.UserName;
    };
    AuthProvider.prototype.loginUser = function (email, password) {
        return __WEBPACK_IMPORTED_MODULE_1_firebase___default.a.auth().signInWithEmailAndPassword(email, password);
    };
    AuthProvider.prototype.signupUser = function (email, password) {
        return __awaiter(this, void 0, void 0, function () {
            var newUser, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_1_firebase___default.a
                                .auth()
                                .createUserWithEmailAndPassword(email, password)];
                    case 1:
                        newUser = _a.sent();
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_1_firebase___default.a
                                .database()
                                .ref("/SignUpDetails/" + newUser.uid + "/email")
                                .set(email)];
                    case 2:
                        _a.sent();
                        console.log(newUser);
                        return [2 /*return*/, newUser];
                    case 3:
                        error_1 = _a.sent();
                        throw error_1;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    AuthProvider.prototype.resetPassword = function (email) {
        return __WEBPACK_IMPORTED_MODULE_1_firebase___default.a.auth().sendPasswordResetEmail(email);
    };
    AuthProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["a" /* AngularFireDatabase */]])
    ], AuthProvider);
    return AuthProvider;
}());

//# sourceMappingURL=provider.js.map

/***/ }),

/***/ 357:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(358);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(376);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 376:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(341);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(344);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__(540);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angularfire2__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_angularfire2_auth__ = __webpack_require__(356);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_angularfire2_database__ = __webpack_require__(281);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__firebase_credentials__ = __webpack_require__(345);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_common_http__ = __webpack_require__(580);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__providers_provider_provider__ = __webpack_require__(355);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_angular2_recaptcha__ = __webpack_require__(583);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_angular2_recaptcha___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_12_angular2_recaptcha__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












//import {  WindowService } from '../providers/window/window';

var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/afternoon/afternoon.module#AfternoonPageModule', name: 'AfternoonPage', segment: 'afternoon', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/appointment-list/appointment-list.module#AppointmentListPageModule', name: 'AppointmentListPage', segment: 'appointment-list', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/cancelbooking/cancelbooking.module#CancelbookingPageModule', name: 'CancelbookingPage', segment: 'cancelbooking', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/dr-gopalrao/dr-gopalrao.module#DrGopalraoPageModule', name: 'DrGopalraoPage', segment: 'dr-gopalrao', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/dr-login/dr-login.module#DrLoginPageModule', name: 'DrLoginPage', segment: 'dr-login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/drprofile-details/drprofile-details.module#DrprofileDetailsPageModule', name: 'DrprofileDetailsPage', segment: 'drprofile-details', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/edit/edit.module#EditPageModule', name: 'EditPage', segment: 'edit', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/evening/evening.module#EveningPageModule', name: 'EveningPage', segment: 'evening', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/home/home.module#HomePageModule', name: 'HomePage', segment: 'home', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/hosptaldetails/hosptaldetails.module#HosptaldetailsPageModule', name: 'HosptaldetailsPage', segment: 'hosptaldetails', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/morningslot/morningslot.module#MorningslotPageModule', name: 'MorningslotPage', segment: 'morningslot', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/patient-drprofile/patient-drprofile.module#PatientDrprofilePageModule', name: 'PatientDrprofilePage', segment: 'patient-drprofile', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/reset-password/reset-password.module#ResetPasswordPageModule', name: 'ResetPasswordPage', segment: 'reset-password', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/settings/settings.module#SettingsPageModule', name: 'SettingsPage', segment: 'settings', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/signup/signup.module#SignupPageModule', name: 'SignupPage', segment: 'signup', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/slotbooking/slotbooking.module#SlotbookingPageModule', name: 'SlotbookingPage', segment: 'slotbooking', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/specialist/specialist.module#SpecialistPageModule', name: 'SpecialistPage', segment: 'specialist', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_6_angularfire2__["a" /* AngularFireModule */].initializeApp(__WEBPACK_IMPORTED_MODULE_9__firebase_credentials__["a" /* config */]),
                __WEBPACK_IMPORTED_MODULE_7_angularfire2_auth__["b" /* AngularFireAuthModule */],
                __WEBPACK_IMPORTED_MODULE_8_angularfire2_database__["b" /* AngularFireDatabaseModule */],
                __WEBPACK_IMPORTED_MODULE_12_angular2_recaptcha__["ReCaptchaModule"]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_10__angular_common_http__["a" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_11__providers_provider_provider__["a" /* AuthProvider */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] },
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 540:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(344);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(341);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_firebase__ = __webpack_require__(239);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_firebase__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__firebase_credentials__ = __webpack_require__(345);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_firebase_firestore__ = __webpack_require__(541);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_firebase_firestore___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_firebase_firestore__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = 'LoginPage';
        platform.ready().then(function () {
            __WEBPACK_IMPORTED_MODULE_4_firebase___default.a.initializeApp(__WEBPACK_IMPORTED_MODULE_5__firebase_credentials__["a" /* config */]);
            //  App.initializeApp(config);
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
        this.Menu = [
            { title: "Home", icon: 'home', page: 'HomePage' },
            { title: 'Help', icon: 'help-circle', page: 'SettingsPage' },
            { title: 'Signout', icon: 'log-out', page: 'LoginPage' },
        ];
        // const unsubscribe = firebase.auth().onAuthStateChanged(user => {
        //   if (!user) {
        //     this.rootPage = 'LoginPage';
        //     unsubscribe();
        //   } else {
        //     this.rootPage = 'HomePage';
        //     unsubscribe();
        //   }
        // });
    }
    MyApp.prototype.openPage = function (root) {
        if (root.title == "Signout") {
            this.nav.setRoot(root.page);
        }
        this.nav.push(root.page, { 'page': root.title });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/home/krishna/krishna/projects/Dr.Application/src/app/app.html"*/'<ion-nav [root]="rootPage" #content swipeBackEnabled="false" main name="app"></ion-nav>\n\n<ion-menu [content]="content" id=\'Menu\' side="right" >\n        \n              <ion-content class="rightmenu" style="background: lightcyan;" >\n                  <img src="./assets/imgs/newdr.png" />\n                <ion-list>\n                  <button ion-item menuClose *ngFor="let p of Menu" (click)="openPage(p)" >\n                        <ion-icon class="menu-icon" name="{{p.icon}}">\n                                <span class="span-txt">{{p.title}}</span>\n                        </ion-icon>\n                    \n                  </button>\n                </ion-list>\n              </ion-content>\n        \n        \n            </ion-menu>\n    \n       '/*ion-inline-end:"/home/krishna/krishna/projects/Dr.Application/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ })

},[357]);
//# sourceMappingURL=main.js.map