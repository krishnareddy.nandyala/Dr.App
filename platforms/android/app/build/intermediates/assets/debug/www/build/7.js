webpackJsonp([7],{

/***/ 596:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MorningslotPageModule", function() { return MorningslotPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__morningslot__ = __webpack_require__(615);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MorningslotPageModule = (function () {
    function MorningslotPageModule() {
    }
    MorningslotPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__morningslot__["a" /* MorningslotPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__morningslot__["a" /* MorningslotPage */]),
            ],
        })
    ], MorningslotPageModule);
    return MorningslotPageModule;
}());

//# sourceMappingURL=morningslot.module.js.map

/***/ }),

/***/ 615:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MorningslotPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_provider_provider__ = __webpack_require__(355);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MorningslotPage = (function () {
    function MorningslotPage(navCtrl, navParams, toast, db, alert) {
        //var krishna = navParams.data;
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toast = toast;
        this.db = db;
        this.alert = alert;
        this.bookingHistory = [];
        this.Data = {
            doctor: "",
            date: "",
            user: "",
            //Organisation: "",
            // AppointmentType?:string,
            patientname: "",
            selctedslotds: "",
            booking: "",
        };
        this.DailyTimeList = [
            {
                time: "8.00 AM",
                Organisation: "",
                date: this.Data.date,
                doctor: this.Data.doctor,
                status: true
            },
            {
                time: "8.30 AM",
                Organisation: "",
                date: this.Data.date,
                doctor: this.Data.doctor,
                status: true
            },
            {
                time: "9.00 AM",
                Organisation: "",
                date: this.Data.date,
                doctor: this.Data.doctor,
                status: true
            },
            {
                time: "9.30 AM",
                Organisation: "",
                date: this.Data.date,
                doctor: this.Data.doctor,
                status: true
            },
            {
                time: "10.00 AM",
                Organisation: "",
                date: this.Data.date,
                doctor: this.Data.doctor,
                status: true
            },
            {
                time: "10.30 AM",
                Organisation: "",
                date: this.Data.date,
                doctor: this.Data.doctor,
                status: true
            },
            {
                time: "11.00 AM",
                Organisation: "",
                date: this.Data.date,
                doctor: this.Data.doctor,
                status: true
            },
            {
                time: "11.30 AM",
                Organisation: "",
                date: this.Data.date,
                doctor: this.Data.doctor,
                status: true
            },
            {
                time: "12.00 AM",
                Organisation: "",
                date: this.Data.date,
                doctor: this.Data.doctor,
                status: true
            }
        ];
        this.Timing = [];
        this.BookedSlot = {};
        this.BookingInfromationn = {
            booking: '',
        };
        this.temp = [];
        this.AppointmentDetails = []; //array1
        //console.log('ionViewDidLoad krishna',krishna);
        this.data = this.navParams.get('details');
        this.selctedslotdss = []; //array2
        this.Data.user = this.navParams.get("userEmail");
        // this.Data.useremail=this.user;
        this.info = this.navParams.get("details");
        this.Data.doctor = this.info.doctor;
        this.Data.date = this.info.date;
        // this.Data.user=this.info.userEmail;
        this.DailyTimeList.forEach(function (x) {
            x.doctor = _this.Data.doctor;
            x.date = _this.Data.date;
        });
    }
    MorningslotPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MorningslotPage');
        this.BookingData();
    };
    MorningslotPage.prototype.addItem = function (BookedSlot) {
        var _this = this;
        this.db.addUser(BookedSlot).then(function (ref) {
            console.log(ref.key);
            _this.navCtrl.push('HomePage', { key: ref.key });
        });
    };
    MorningslotPage.prototype.AddUserName = function (patientname) {
        this.db.addUserName(patientname).then(function (res) {
            console.log(res.key);
            //return res;
        });
    };
    MorningslotPage.prototype.BookingData = function () {
        var _this = this;
        this.db.getUserDetails().query.on('value', function (results) {
            var BookingList = results.val();
            var y = [];
            for (var z in BookingList) {
                y.push(z);
            }
            for (var i = 0; i < y.length; i++) {
                var sss = BookingList[y[i]];
                console.log(sss);
                var time = void 0;
                if (BookingList[y[i]][1]) {
                    time = BookingList[y[i]][1].time;
                }
                else {
                    time = "";
                }
                var obj = {
                    //  Organisation:BookingList[y[i]][0].booking[0].Organisation,
                    doctor: BookingList[y[i]][0].doctor,
                    date: BookingList[y[i]][0].date,
                    time: time
                };
                _this.bookingHistory.push(obj);
            }
            // this.DailyTimeList.forEach(x => {
            //   x.doctor=this.temp[0].doctor;
            //   x.date = this.temp[0].date;
            // })
            _this.bookingHistory.forEach(function (x) {
                _this.DailyTimeList.forEach(function (y) {
                    if (x.doctor == y.doctor && x.date == y.date && x.time == y.time) {
                        y.status = false;
                    }
                });
            });
            console.log("------AA-----------");
            console.log(_this.bookingHistory);
            console.log("-----------------");
            console.log("--------BB---------");
            console.log(_this.DailyTimeList);
            console.log("-----------------");
        });
    };
    MorningslotPage.prototype.selectTimeSlot = function (a) {
        this.selctedslotdss.push({ time: a });
    };
    MorningslotPage.prototype.submit = function () {
        this.AppointmentDetails.push(this.Data);
        this.BookedSlot = this.AppointmentDetails.concat(this.selctedslotdss);
        console.log("this.BookedSlot", this.BookedSlot);
        console.log("username:" + this.Data.patientname);
        //this.AddUserName(this.patientname);
        this.addItem(this.BookedSlot);
        this.navCtrl.setRoot('HomePage');
        console.info("list", this.DailyTimeList);
    };
    MorningslotPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-morningslot',template:/*ion-inline-start:"/home/krishna/krishna/projects/Dr.Application/src/pages/morningslot/morningslot.html"*/'<ion-header>\n\n  <ion-navbar color="bluegreen">\n    <ion-title>slots</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <form #SlotFillingForm="ngForm">\n\n    <p color="light">Available Slots Are</p>\n\n    <span *ngFor="let DT of DailyTimeList">\n\n      <button [disabled]="!DT.status" ion-button class="first-input" (click)="selectTimeSlot(DT.time)">{{DT.time}}</button>\n    </span>\n\n\n\n    <br>\n    <br>\n    <br>\n    <div>\n      <ion-item>\n        <ion-label floating color="bluegreen">Patient Name</ion-label>\n        <ion-input required type="text" name="patientname" [(ngModel)]="Data.patientname"></ion-input>\n      </ion-item>\n    </div>\n    <div>\n      <ion-item>\n        <ion-label floating color="bluegreen">Age</ion-label>\n        <ion-input  type="number" maxlength="2"></ion-input>\n      </ion-item>\n    </div>\n\n    <div>\n      <ion-item>\n        <ion-label floating color="bluegreen">Gender</ion-label>\n        <ion-select required interface="popover" name="sex" [(ngModel)]="gender">\n          <ion-option value="m">Male</ion-option>\n          <ion-option value="f">Female</ion-option>\n        </ion-select>\n      </ion-item>\n    </div>\n    <br>\n    <br>\n    <ion-row>\n      <button ion-button round (click)="submit()" [disabled]="SlotFillingForm.invalid" class="booking-button">Book Now</button>\n    </ion-row>\n  </form>\n</ion-content>'/*ion-inline-end:"/home/krishna/krishna/projects/Dr.Application/src/pages/morningslot/morningslot.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_provider_provider__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], MorningslotPage);
    return MorningslotPage;
}());

//# sourceMappingURL=morningslot.js.map

/***/ })

});
//# sourceMappingURL=7.js.map