webpackJsonp([2],{

/***/ 595:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login__ = __webpack_require__(614);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var LoginPageModule = (function () {
    function LoginPageModule() {
    }
    LoginPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */]),
            ],
        })
    ], LoginPageModule);
    return LoginPageModule;
}());

//# sourceMappingURL=login.module.js.map

/***/ }),

/***/ 603:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EmailValidator; });
var EmailValidator = (function () {
    function EmailValidator() {
    }
    EmailValidator.isValid = function (control) {
        var re = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/.test(control.value);
        if (re) {
            return null;
        }
        return {
            invalidEmail: true
        };
    };
    return EmailValidator;
}());

//# sourceMappingURL=email.js.map

/***/ }),

/***/ 614:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_provider_provider__ = __webpack_require__(355);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__ = __webpack_require__(356);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__validators_email__ = __webpack_require__(603);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






var LoginPage = (function () {
    //  public userlogin: boolean;
    function LoginPage(navCtrl, navParams, alert, provider, afauth, formBuilder) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alert = alert;
        this.provider = provider;
        this.afauth = afauth;
        this.formBuilder = formBuilder;
        this.loginEmail = {
            Email: ''
        };
        this.images = ['2.jpg', '3.jpg', '4.jpg'];
        this.loginForm = formBuilder.group({
            email: [
                '',
                __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_5__validators_email__["a" /* EmailValidator */].isValid])
            ],
            password: [
                '',
                __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].minLength(6)])
            ]
        });
    }
    LoginPage.prototype.loginUser = function () {
        return __awaiter(this, void 0, void 0, function () {
            var email, password, loginUser, loginUser, loginUser, loginUser, loginUser, loginUser, loginUser, loginUser, loginUser, Email, error_1, alert_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        email = this.loginForm.value.email;
                        password = this.loginForm.value.password;
                        if (!(email == 'doctor@gmail.com' && password == 'doctor')) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.provider.
                                loginUser(email, password)];
                    case 1:
                        loginUser = _a.sent();
                        this.navCtrl.setRoot('DrProfilePage');
                        return [3 /*break*/, 19];
                    case 2:
                        if (!(email == 'suresh@cardiologist.com' && password == 'suresh')) return [3 /*break*/, 4];
                        return [4 /*yield*/, this.provider.
                                loginUser(email, password)];
                    case 3:
                        loginUser = _a.sent();
                        this.navCtrl.setRoot('AppointmentListPage', { 'details': email });
                        return [3 /*break*/, 19];
                    case 4:
                        if (!(email == 'sameer@neuro.com' && password == 'sameer')) return [3 /*break*/, 6];
                        return [4 /*yield*/, this.provider.
                                loginUser(email, password)];
                    case 5:
                        loginUser = _a.sent();
                        this.navCtrl.setRoot('AppointmentListPage', { 'details': email });
                        return [3 /*break*/, 19];
                    case 6:
                        if (!(email == 'ananth@Nephrology.com' && password == 'ananth')) return [3 /*break*/, 8];
                        return [4 /*yield*/, this.provider.
                                loginUser(email, password)];
                    case 7:
                        loginUser = _a.sent();
                        this.navCtrl.setRoot('AppointmentListPage', { 'details': email });
                        return [3 /*break*/, 19];
                    case 8:
                        if (!(email == 'srinivas@Gastroenterology.com' && password == 'srinivas')) return [3 /*break*/, 10];
                        return [4 /*yield*/, this.provider.
                                loginUser(email, password)];
                    case 9:
                        loginUser = _a.sent();
                        this.navCtrl.setRoot('AppointmentListPage', { 'details': email });
                        return [3 /*break*/, 19];
                    case 10:
                        if (!(email == 'harsha@cardiologist.com' && password == 'harsha')) return [3 /*break*/, 12];
                        return [4 /*yield*/, this.provider.
                                loginUser(email, password)];
                    case 11:
                        loginUser = _a.sent();
                        this.navCtrl.setRoot('AppointmentListPage', { 'details': email });
                        return [3 /*break*/, 19];
                    case 12:
                        if (!(email == 'gopalrao@Pulmonology.com' && password == 'gopalrao')) return [3 /*break*/, 14];
                        return [4 /*yield*/, this.provider.
                                loginUser(email, password)];
                    case 13:
                        loginUser = _a.sent();
                        this.navCtrl.setRoot('AppointmentListPage', { 'details': email });
                        return [3 /*break*/, 19];
                    case 14:
                        if (!(email == 'venkateswarao@Nephrology.com' && password == 'venkateswarao')) return [3 /*break*/, 16];
                        return [4 /*yield*/, this.provider.
                                loginUser(email, password)];
                    case 15:
                        loginUser = _a.sent();
                        this.navCtrl.setRoot('DrLoginPage');
                        return [3 /*break*/, 19];
                    case 16:
                        _a.trys.push([16, 18, , 19]);
                        return [4 /*yield*/, this.provider.
                                loginUser(email, password)];
                    case 17:
                        loginUser = _a.sent();
                        Email = email;
                        this.loginEmail.Email = Email;
                        console.log("krishna", this.loginEmail);
                        this.navCtrl.setRoot('HomePage', { 'userEmail': this.loginEmail.Email });
                        return [3 /*break*/, 19];
                    case 18:
                        error_1 = _a.sent();
                        alert_1 = this.alert.create({
                            title: '!oops',
                            subTitle: error_1.message,
                            buttons: [{ text: 'Ok', role: 'cancel' }]
                        });
                        alert_1.present();
                        return [3 /*break*/, 19];
                    case 19: return [2 /*return*/];
                }
            });
        });
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-login',template:/*ion-inline-start:"/home/krishna/krishna/projects/Dr.Application/src/pages/login/login.html"*/'<ion-header>\n\n  <ion-navbar color="bluegreen">\n    <ion-row>\n      <img class="grid-image" src="./assets/icon/doct.png">\n      <ion-title>Doctor App</ion-title>\n    </ion-row>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content class="auth-page">\n\n  <!-- <ion-slides class="image-slider" loop="true" autoplay="5000" slidesPerView="1">\n    <ion-slide *ngFor="let img of images">\n      <img src="assets/imgs/{{img}}" class="thumb-img" imageViewer/>\n    </ion-slide>\n  </ion-slides> -->\n  <div>\n    <img src="./assets/imgs/hosptal.jpg" />\n  </div>\n  <form class="login-center" [formGroup]="loginForm" novalidate>\n    <ion-item class="Back" [class.invalid]="!loginForm.controls.email.valid && loginForm.controls.email.touched" [class.valid]="loginForm.controls.email.valid && loginForm.controls.email.touched">\n      <ion-label color="light" floating> Email</ion-label>\n\n      <ion-input formControlName="email" type="email">\n      </ion-input>\n    </ion-item>\n    <!-- <ion-item class="error-message" *ngIf="!loginForm.controls.email.valid &&\n     loginForm.controls.email.touched">\n      <p>Please enter a valid email address.</p>\n    </ion-item> -->\n    \n        <ion-item class="Back" [class.invalid]="!loginForm.controls.password.valid && loginForm.controls.password.touched" [class.valid]="loginForm.controls.password.valid && loginForm.controls.password.touched">\n\n\n        \n    <ion-label color="light" floating>Password</ion-label>\n      <ion-input formControlName="password" type="password" >\n      </ion-input>\n    </ion-item>\n    <!-- <ion-item class="error-message" *ngIf="!loginForm.controls.password.valid\n    && loginForm.controls.password.touched">\n      <p>Your password needs more than 6 characters.</p>\n    </ion-item> -->\n    <br>\n    <!-- //////////////////////////////////////////////////// -->\n    <ion-row style="margin-bottom: 12px;">\n      <ion-col class="signup" navPush="SignupPage">\n        Create new\n      </ion-col>\n      <ion-col class="Forgotpwd" navPush="ResetPasswordPage">\n        Forgotpassword?\n      </ion-col>\n    </ion-row>\n    <!-- /////////////////////////////////////////////////////// -->\n    <button ion-button round class="LoginButton" (click)="loginUser()" [disabled]="!loginForm.valid">\n      Log In\n    </button>\n    <!-- <ion-row>\n      <ion-col col-1>\n\n\n      </ion-col>\n      <ion-col col-9>\n            <button ion-button block clear color="bluegreen" navPush="SignupPage">\n                <ion-icon color="bluegreen" class="accimg" name="person-add"></ion-icon> &nbsp;&nbsp;&nbsp; Create a new account\n              </button>\n        </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col col-1>\n\n\n      </ion-col>\n      <ion-col col-9>\n        <button ion-button block clear color="bluegreen" navPush="ResetPasswordPage">\n          <ion-icon color="bluegreen" class="accimg" name="briefcase"></ion-icon> &nbsp;&nbsp;&nbsp; I forgot my password\n        </button>\n      </ion-col>\n    </ion-row> -->\n\n\n  </form>\n</ion-content>'/*ion-inline-end:"/home/krishna/krishna/projects/Dr.Application/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_2__providers_provider_provider__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__["a" /* AngularFireAuth */], __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormBuilder"]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ })

});
//# sourceMappingURL=2.js.map