import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, App } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import firebase from 'firebase';
import { config } from './firebase.credentials';
import { HomePage } from '../pages/home/home';
import 'firebase/firestore';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:string = 'LoginPage';

 

  @ViewChild(Nav) nav: Nav;
  Menu: { title: string; icon: string; page: string; }[];
  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {


    platform.ready().then(() => {
      firebase.initializeApp(config);
    //  App.initializeApp(config);
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });

    this.Menu = [
      { title: "Home", icon: 'home', page: 'HomePage' },
      { title: 'Help', icon: 'help-circle', page: 'SettingsPage' }
,      { title: 'Signout', icon: 'log-out', page: 'LoginPage' },

      // { title: "Settings", icon: 'settings', page: 'SettingsPage' },
     
    ];
   
    // const unsubscribe = firebase.auth().onAuthStateChanged(user => {
    //   if (!user) {
    //     this.rootPage = 'LoginPage';
    //     unsubscribe();
    //   } else {
    //     this.rootPage = 'HomePage';
    //     unsubscribe();
    //   }
    // });

  }

  openPage(root) {
    if (root.title == "Signout") {
      this.nav.setRoot(root.page)
    }
    this.nav.push(root.page, { 'page': root.title });
  }


}

