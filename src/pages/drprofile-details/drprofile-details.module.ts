import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DrprofileDetailsPage } from './drprofile-details';

@NgModule({
  declarations: [
    DrprofileDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(DrprofileDetailsPage),
  ],
})
export class DrprofileDetailsPageModule {}
