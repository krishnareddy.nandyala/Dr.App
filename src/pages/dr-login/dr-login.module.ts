import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DrLoginPage } from './dr-login';

@NgModule({
  declarations: [
    DrLoginPage,
  ],
  imports: [
    IonicPageModule.forChild(DrLoginPage),
  ],
})
export class DrLoginPageModule {}
