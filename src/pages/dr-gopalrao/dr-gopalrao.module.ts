import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DrGopalraoPage } from './dr-gopalrao';

@NgModule({
  declarations: [
    DrGopalraoPage,
  ],
  imports: [
    IonicPageModule.forChild(DrGopalraoPage),
  ],
})
export class DrGopalraoPageModule {}
