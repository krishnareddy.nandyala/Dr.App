import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AfternoonPage } from './afternoon';

@NgModule({
  declarations: [
    AfternoonPage,
  ],
  imports: [
    IonicPageModule.forChild(AfternoonPage),
  ],
})
export class AfternoonPageModule {}
