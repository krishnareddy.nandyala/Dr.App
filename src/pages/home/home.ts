import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  user: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.user=this.navParams.get("userEmail")
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }
  AboutUs() {
    this.navCtrl.push('HosptaldetailsPage');
  }
  specialist() {
    this.navCtrl.push('SpecialistPage',{'userEmail':this.user});
  }
  cancelBooking()
{
  this.navCtrl.push('CancelbookingPage',{'userEmail':this.user});
}
}

