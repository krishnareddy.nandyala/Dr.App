import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Alert } from 'ionic-angular';
import firebase from 'firebase';
import { AuthProvider } from '../../providers/provider/provider';
import { AngularFireDatabase } from 'angularfire2/database';
import {AngularFireAuth } from 'angularfire2/auth';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ReCaptchaModule } from 'angular2-recaptcha'
import { EmailValidator } from '../../validators/email';
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  email: any;
  windowRef: any;
 
  loginEmail={
    Email:''
  }
  public loginForm: FormGroup;
  images = ['2.jpg', '3.jpg', '4.jpg']
  public recaptchaVerifier: firebase.auth.RecaptchaVerifier;
  //  public userlogin: boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public alert: AlertController,public provider:AuthProvider,
  public afauth:AngularFireAuth,public formBuilder: FormBuilder,
) {
  this.loginForm = formBuilder.group({
    email: [
      '',
      Validators.compose([Validators.required, EmailValidator.isValid])
    ],
    password: [
      '',
      Validators.compose([Validators.required, Validators.minLength(6)])
    ]
  });
  }
 
 
  async loginUser(): Promise<void> {
    // const loading: Loading = this.loadingCtrl.create();
    // loading.present();
  
    var email = this.loginForm.value.email;
    var password = this.loginForm.value.password;
    if(email=='doctor@gmail.com' && password=='doctor'){

      var loginUser: firebase.User = await this.provider.
      loginUser(email,password);
     
    this.navCtrl.setRoot('DrProfilePage')

        }
       else if (email=='suresh@cardiologist.com' && password=='suresh') {
        var loginUser: firebase.User = await this.provider.
        loginUser(email,password);
       
      this.navCtrl.setRoot('AppointmentListPage',{'details':email})

       } 
       else if (email=='sameer@neuro.com' && password=='sameer') {
        var loginUser: firebase.User = await this.provider.
        loginUser(email,password);
       
      this.navCtrl.setRoot('AppointmentListPage',{'details':email})
       } 
       else if (email=='ananth@Nephrology.com' && password=='ananth') {
        var loginUser: firebase.User = await this.provider.
        loginUser(email,password);
       
      this.navCtrl.setRoot('AppointmentListPage',{'details':email})
       } 
       else if (email=='srinivas@Gastroenterology.com' && password=='srinivas') {
        var loginUser: firebase.User = await this.provider.
        loginUser(email,password);
       
      this.navCtrl.setRoot('AppointmentListPage',{'details':email})
       } 
       else if (email=='harsha@cardiologist.com' && password=='harsha') {
        var loginUser: firebase.User = await this.provider.
        loginUser(email,password);
       
      this.navCtrl.setRoot('AppointmentListPage',{'details':email})
       } 
       else if (email=='gopalrao@Pulmonology.com' && password=='gopalrao') {
        var loginUser: firebase.User = await this.provider.
        loginUser(email,password);
       
      this.navCtrl.setRoot('AppointmentListPage',{'details':email})
       } 
       else if (email=='venkateswarao@Nephrology.com' && password=='venkateswarao') {
        var loginUser: firebase.User = await this.provider.
        loginUser(email,password);
       
      this.navCtrl.setRoot('DrLoginPage')
       } 

       
         else{
          try {
            var loginUser: firebase.User = await this.provider.
            loginUser(email,password);
           
            var Email = email;
            this.loginEmail.Email=Email;
        console.log("krishna",this.loginEmail)
       
            this.navCtrl.setRoot('HomePage',{'userEmail':this.loginEmail.Email});
          
          }
          catch (error) {
            
             const alert: Alert = this.alert.create({
               title:'!oops',
               subTitle: error.message,
               buttons: [{ text: 'Ok', role: 'cancel' }]
             });
             alert.present();
           }
         }
   

  }

}
