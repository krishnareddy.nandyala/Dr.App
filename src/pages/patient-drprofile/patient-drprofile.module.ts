import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PatientDrprofilePage } from './patient-drprofile';

@NgModule({
  declarations: [
    PatientDrprofilePage,
  ],
  imports: [
    IonicPageModule.forChild(PatientDrprofilePage),
  ],
})
export class PatientDrprofilePageModule {}
