import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EveningPage } from './evening';

@NgModule({
  declarations: [
    EveningPage,
  ],
  imports: [
    IonicPageModule.forChild(EveningPage),
  ],
})
export class EveningPageModule {}
