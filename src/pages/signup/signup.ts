import { Component } from '@angular/core';
import {
  Alert,
  AlertController,
  IonicPage,
  Loading,
  LoadingController,
  NavController
} from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { HomePage } from '../home/home';
import { EmailValidator } from '../../validators/email';
import firebase from 'firebase/app';
import { AuthProvider } from '../../providers/provider/provider';

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {
  public signupForm: FormGroup;
  constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public authProvider: AuthProvider,
    formBuilder: FormBuilder
  ) {
    this.signupForm = formBuilder.group({
      email: [
        '',
        Validators.compose([Validators.required, EmailValidator.isValid])
      ],
      password: [
        '',
        Validators.compose([Validators.required, Validators.minLength(6)])
      ]
    });
  }

  async signupUser(): Promise<void> {
   

      const email = this.signupForm.value.email;
      const password = this.signupForm.value.password;

      try {
        const signupUser: firebase.User = await this.authProvider.signupUser(
          email,
          password
        );
        console.log(signupUser)
       // await loading.dismiss();
        this.navCtrl.setRoot('LoginPage');
      } catch (error) {
       // await loading.dismiss();
        const alert: Alert = this.alertCtrl.create({
          message: error.message,
          buttons: [{ text: 'Ok', role: 'cancel' }]
        });
        alert.present();
      }
    //}
  }
}
