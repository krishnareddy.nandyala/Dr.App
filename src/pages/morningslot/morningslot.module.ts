import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MorningslotPage } from './morningslot';

@NgModule({
  declarations: [
    MorningslotPage,
  ],
  imports: [
    IonicPageModule.forChild(MorningslotPage),
  ],
})
export class MorningslotPageModule {}
