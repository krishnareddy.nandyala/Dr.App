import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController } from 'ionic-angular';
import { AuthProvider } from '../../providers/provider/provider';
import { data } from '../../model/data.model';


@IonicPage()
@Component({
  selector: 'page-morningslot',
  templateUrl: 'morningslot.html',
})
export class MorningslotPage {


  user: any;
  info: any;
  data: any;
  
  selctedslotds: any;
  selctedslotdss: any;
  bookingHistory = [];

  

  Data: data = {
    doctor: "",
    date: "",
    user:"",
    //Organisation: "",
    // AppointmentType?:string,
    patientname: "",
    selctedslotds: "",
    booking: "",

  }


  DailyTimeList = [
    {
      time: "8.00 AM",
      Organisation: "",
      date: this.Data.date,
      doctor: this.Data.doctor,
      status: true
    },
    {
      time: "8.30 AM",
      Organisation: "",
      date: this.Data.date,
      doctor: this.Data.doctor,
      status: true

    },
    {
      time: "9.00 AM",
      Organisation: "",
      date: this.Data.date,
      doctor: this.Data.doctor,
      status: true
    },
    {
      time: "9.30 AM",
      Organisation: "",
      date: this.Data.date,
      doctor: this.Data.doctor,
      status: true
    },
    {
      time: "10.00 AM",
      Organisation: "",
      date: this.Data.date,
      doctor: this.Data.doctor,
      status: true
    },
    {
      time: "10.30 AM",
      Organisation: "",
      date: this.Data.date,
      doctor: this.Data.doctor,
      status: true
    },
    {
      time: "11.00 AM",
      Organisation: "",
      date: this.Data.date,
      doctor: this.Data.doctor,
      status: true
    },
    {
      time: "11.30 AM",
      Organisation: "",
      date: this.Data.date,
      doctor: this.Data.doctor,
      status: true
    },
    {
      time: "12.00 AM",
      Organisation: "",
      date: this.Data.date,
      doctor: this.Data.doctor,
      status: true
    }
  ]



  Timing = [

  ]
  BookedSlot = {

  }
  BookingInfromationn: data = {

    booking: '',

  }

  temp = [];


  AppointmentDetails = [] //array1
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public db: AuthProvider,
    public alert: AlertController) {
     //var krishna = navParams.data;
     
     //console.log('ionViewDidLoad krishna',krishna);
    this.data = this.navParams.get('details');
    this.selctedslotdss = []; //array2


     this.Data.user = this.navParams.get("userEmail");
    // this.Data.useremail=this.user;

    this.info = this.navParams.get("details");
    this.Data.doctor = this.info.doctor;
    this.Data.date = this.info.date;
    // this.Data.user=this.info.userEmail;

    this.DailyTimeList.forEach(x=>{
      x.doctor=this.Data.doctor;
      x.date=this.Data.date
    })
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad MorningslotPage');
    this.BookingData()
  }
  addItem(BookedSlot: data) {
    this.db.addUser(BookedSlot).then(ref => {
      console.log(ref.key);
      this.navCtrl.push('HomePage', { key: ref.key });

    })
  }

  AddUserName(patientname) {
    this.db.addUserName(patientname).then(res => {
      console.log(res.key)
      //return res;
    })
  }

  BookingData() {

    this.db.getUserDetails().query.on('value', results => {
      var BookingList = results.val();
      var y = [];
      for (var z in BookingList) {
        y.push(z)
      }
      for (var i = 0; i < y.length; i++) {

        var sss=BookingList[y[i]]
        console.log(sss)
let time;
        if(BookingList[y[i]][1])
        {
          time=BookingList[y[i]][1].time;
        }
        else{
          time="";
        }
        var obj =
          {
            //  Organisation:BookingList[y[i]][0].booking[0].Organisation,
            doctor:BookingList[y[i]][0].doctor,
            date: BookingList[y[i]][0].date,
             time: time
          }
        this.bookingHistory.push(obj)
      }


      // this.DailyTimeList.forEach(x => {
       
      //   x.doctor=this.temp[0].doctor;
      //   x.date = this.temp[0].date;

      // })


      this.bookingHistory.forEach(x => {
        this.DailyTimeList.forEach(y => {
          if (x.doctor ==y.doctor && x.date == y.date && x.time == y.time) {
            y.status = false

          }

        })

      })
      console.log("------AA-----------")
      console.log(this.bookingHistory)
      console.log("-----------------")
      console.log("--------BB---------")
      console.log(this.DailyTimeList)
      console.log("-----------------")

    });

  }

  selectTimeSlot(a) {
    this.selctedslotdss.push({ time: a });
  }

  
  submit() {
    this.AppointmentDetails.push(this.Data);
    this.BookedSlot = this.AppointmentDetails.concat(this.selctedslotdss);
    console.log("this.BookedSlot", this.BookedSlot);
    console.log("username:" + this.Data.patientname)

    //this.AddUserName(this.patientname);
    this.addItem(this.BookedSlot);

    this.navCtrl.setRoot('HomePage');
    console.info("list",this.DailyTimeList);
  }




}
