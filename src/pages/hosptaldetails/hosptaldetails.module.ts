import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HosptaldetailsPage } from './hosptaldetails';

@NgModule({
  declarations: [
    HosptaldetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(HosptaldetailsPage),
  ],
})
export class HosptaldetailsPageModule {}
