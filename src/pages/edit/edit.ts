import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthProvider } from '../../providers/provider/provider';



@IonicPage()
@Component({
  selector: 'page-edit',
  templateUrl: 'edit.html',
})
export class EditPage {
details={
  qualification:'',
  experience:'',
  language:'',
  key:''
}
  constructor(public navCtrl: NavController, public navParams: NavParams,
  public provider:AuthProvider) {
  }

  ionViewDidLoad() {
   this.details = this.navParams.get('item');
    console.log(this.navParams.get('item'))
    console.log(this.details);
  }

  save(details){
this.provider.updatedr(details).then(ref=>{
  // console.log(ref.key)
   this.navCtrl.push('DrprofileDetailsPage')
})
  }

}
