import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the SpecialistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-specialist',
  templateUrl: 'specialist.html',
})
export class SpecialistPage {

  user: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.user=this.navParams.get("userEmail")
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SpecialistPage');
  }

  openCardiologyPage()
{
  this.navCtrl.push('SlotbookingPage',{selectdoctors:'Cardiologist', 'userEmail':this.user})
}
  openNephrologyPage()
{
  this.navCtrl.push('SlotbookingPage',{selectdoctors:'Nephrologist','userEmail':this.user})
}
openPulmonologyPage()
{
  this.navCtrl.push('SlotbookingPage',{selectdoctors:'Pulmonologist','userEmail':this.user})
}
openGastroenterologyPage()
{
  this.navCtrl.push('SlotbookingPage',{selectdoctors:'Gastroenterologist','userEmail':this.user})
}
openNeurologyPage()
{
  this.navCtrl.push('SlotbookingPage',{selectdoctors:'Neurologist','userEmail':this.user})
}
}
