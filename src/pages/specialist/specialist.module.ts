import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SpecialistPage } from './specialist';

@NgModule({
  declarations: [
    SpecialistPage,
  ],
  imports: [
    IonicPageModule.forChild(SpecialistPage),
  ],
})
export class SpecialistPageModule {}
