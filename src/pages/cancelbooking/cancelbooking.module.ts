import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CancelbookingPage } from './cancelbooking';

@NgModule({
  declarations: [
    CancelbookingPage,
  ],
  imports: [
    IonicPageModule.forChild(CancelbookingPage),
  ],
})
export class CancelbookingPageModule {}
