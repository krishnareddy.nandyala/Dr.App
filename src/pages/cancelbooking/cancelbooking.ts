import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthProvider } from '../../providers/provider/provider';
import { Observable } from 'rxjs/Observable';
import { data } from '../../model/data.model';
import firebase from 'firebase';
import * as moment from 'moment';
import * as _ from 'underscore';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';


@IonicPage()
@Component({
  selector: 'page-cancelbooking',
  templateUrl: 'cancelbooking.html',
})
export class CancelbookingPage {
  
  doctoremail: any;
  patientsList: any[];
  //data:FormGroup;
  edit: boolean = true;
  abc: boolean;
  Edit() {
    this.edit = true;
    this.abc = false;
  }
  DoctorDetails = {
    langE: '',
    langH: '',
    LabgT: '',
    Ecperience: ''
  }
  bookingDetails = [];
  daywiseList = [];

  drprofile: boolean = true;
  appointment: boolean;

  DrProfile() {
    this.drprofile = true;
    this.appointment = false;
  }
  Appointment() {
    this.drprofile = false;
    this.appointment = true;
  }
  // BookingInfromations: Observable<data[]>;
  BookingInfromations = {}
  Data1 = []
  Data = {}
  booked = []
  c = []
  d = []
  names: any[];
  NamesOfusers = [];
  selecteddates: Observable<data[]>;
  selctedslotds: Observable<data[]>;
  BookingInfromation;

  user: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public service: AuthProvider) {

    this.user = this.navParams.get('userEmail');

    this.BookingInfromations = this.service.
      getUserDetails().
      query.on('value', personSnapshot => {
        this.selctedslotds = personSnapshot.val();
        console.log("bookingDetails", personSnapshot.val());

        var d = this.selctedslotds;
        var a = [];
        this.booked = [];
        for (var x in d) {
          a.push(x)
        }
        // console.log(a);
        for (var i = 0; i < a.length; i++) {
          let obj = { id: a[i] }
          var obj2 = {
            date: d[a[i]][0].date,
            doctor: d[a[i]][0].doctor,
            patientname: d[a[i]][0].patientname,
            useremail:d[a[i]][0].user,
            time: d[a[i]][1],
            obj

          }
          this.booked.push(obj2)
          console.log(" this.booked", this.booked);
        }
        this.bookingDetails = this.booked
        console.log("bookingDetails", this.bookingDetails);

      })
    this.Data = this.service.getUserName().query.on('value', Username => {
      this.Data1 = Username.val();
      // console.log(this.Data1); 

      this.names = this.Data1;
      // console.log(this.names)
      var y = [];
      for (var z in this.names) {
        y.push(z)
      }



      for (var j = 0; j < y.length; j++) {


        var name = {
          username: this.names[y[j]]
        }
        this.NamesOfusers.push(name);
      }
      this.bookingDetails.forEach((x, index) => {

        //x.username = this.NamesOfusers[index].username;


      })

      console.log(this.bookingDetails);


    })



  }
  showDetails() {
    this.patientsList = this.bookingDetails.filter(
      x => {
        if (x.useremail == this.user) {
          return x;

        }

      })

  }

  delete(key){

    this.service.removeUser(key);
    // this.navCtrl.push('HomePage')
    this.patientsList = this.bookingDetails.filter(
      x => {
        if (x.useremail == this.user) {
          return x;

        }

      })

        
  }

}