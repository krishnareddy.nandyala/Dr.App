import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController } from 'ionic-angular';
import { MorningslotPage } from '../morningslot/morningslot';
import { data } from '../../model/data.model';
import {  AuthProvider } from '../../providers/provider/provider';
import { Observable } from 'rxjs/Observable';

/**
 * Generated class for the SlotbookingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-slotbooking',
  templateUrl: 'slotbooking.html',
})
export class SlotbookingPage {
  user: any;
  doctors: any;
  item: any;
  BookingInfromations: Observable<data[]>;

  BookingInfromation: any;
  bookingrooms: any;

  
 Data:data={
   doctor:'',
   date:''
 }
  query2=[this.Data];

  constructor(public navCtrl: NavController, public navParams: NavParams,
      public modalCtrl:ModalController,public db:AuthProvider,
      public alert:AlertController) {
  this.doctors=this.navParams.get("selectdoctors");
  this.user=this.navParams.get("userEmail")


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SlotbookingPage');
  }
  booknow(){
    this.navCtrl.push('MorningslotPage',{'details':this.Data,"userEmail":this.user});
     //console.log(this.query2);
   
   }
 

}
