import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SlotbookingPage } from './slotbooking';

@NgModule({
  declarations: [
    SlotbookingPage,
  ],
  imports: [
    IonicPageModule.forChild(SlotbookingPage),
  ],
})
export class SlotbookingPageModule {}
