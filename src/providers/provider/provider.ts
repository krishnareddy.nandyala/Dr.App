import { Injectable } from '@angular/core';
import firebase from 'firebase';
import { AngularFireDatabase } from 'angularfire2/database';
import { data } from '../../model/data.model';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AuthProvider {


  info: any;
  constructor(public database: AngularFireDatabase) {
  }
  public DrDetails = this.database.list('DrDetails')

  public HospitalDetails = this.database.list('Hospotal')

  public UserName = this.database.list('Username');
  public UserAppointmentDetails = this.database.list<data>('UserAppointmentDetails')

  details={
    Hospotal:'',
    branch:'',
    location:'',
    key:''
  }
  addHospitalDetails(details){
    return this.HospitalDetails.push(details)
  } 
  getHospitalDetails(){
    return this.HospitalDetails;
  }
  updateHospitalDetails(details){
    return this.HospitalDetails.update(details.key,details)
  }

  // *************************************

  removeUser(key: string) {
    this.UserAppointmentDetails.remove(key).then(_ => console.log('item deleted!'));
  }


  addDrDetails(details){
    return this.DrDetails.push(details)
  }
  getDrDetails(){
    return this.DrDetails;
  }
  updatedr(details){
    return this.DrDetails.update(details.key,details)
  }
  
  addUser(Data: data) {
    return this.UserAppointmentDetails.push(Data)
  }

  getUserDetails() {
    console.log(this.UserAppointmentDetails);
    return this.UserAppointmentDetails;
  }
  addUserName(patientname) {
    return this.UserName.push(patientname)
  }
  
  getUserName(){
    return this.UserName;
  }


  loginUser(email: string, password: string): Promise<firebase.User> {
    return firebase.auth().signInWithEmailAndPassword(email, password);

  }

  async signupUser(email: string, password: string): Promise<firebase.User> {
    try {
      const newUser: firebase.User = await firebase
        .auth()
        .createUserWithEmailAndPassword(email, password);

      await firebase
        .database()
        .ref(`/SignUpDetails/${newUser.uid}/email`)
        .set(email);
      console.log(newUser);
      return newUser;
    } catch (error) {
      throw error;
    }
  }
  resetPassword(email: string): Promise<void> {
    return firebase.auth().sendPasswordResetEmail(email);
  }
}
