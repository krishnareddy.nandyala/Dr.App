webpackJsonp([12],{

/***/ 590:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DrprofileDetailsPageModule", function() { return DrprofileDetailsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__drprofile_details__ = __webpack_require__(609);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var DrprofileDetailsPageModule = (function () {
    function DrprofileDetailsPageModule() {
    }
    DrprofileDetailsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__drprofile_details__["a" /* DrprofileDetailsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__drprofile_details__["a" /* DrprofileDetailsPage */]),
            ],
        })
    ], DrprofileDetailsPageModule);
    return DrprofileDetailsPageModule;
}());

//# sourceMappingURL=drprofile-details.module.js.map

/***/ }),

/***/ 609:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DrprofileDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_provider_provider__ = __webpack_require__(355);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the DrprofileDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DrprofileDetailsPage = (function () {
    function DrprofileDetailsPage(navCtrl, provider, navParams) {
        this.navCtrl = navCtrl;
        this.provider = provider;
        this.navParams = navParams;
        this.result = this.provider.getDrDetails().snapshotChanges().
            // query.on('value',result=>{
            //   var results=result.val().
            map(function (changes) {
            console.log(changes.map(function (c) { return (__assign({ key: c.payload.key }, c.payload.val())); }));
            return changes.map(function (c) { return (__assign({ key: c.payload.key }, c.payload.val())); });
        });
        console.log("------------------");
        this.drdetailss = this.result;
        console.log(this.drdetailss);
        console.log("------------------");
        //console.log(result.val());
        //   this.drdetailss=results
        //  var key=[]
        //  console.log( this.drdetailss);
        //   for(var i in this.drdetailss ){
        // key.push(i);
        // console.log(key)
        //   }
        //   for (var j=0;j=0;j++){
        //   }
    }
    DrprofileDetailsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DrprofileDetailsPage');
    };
    DrprofileDetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-drprofile-details',template:/*ion-inline-start:"/home/krishna/krishna/projects/Dr.Application/src/pages/drprofile-details/drprofile-details.html"*/'<!--\n  Generated template for the DrprofileDetailsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="bluegreen">\n    <ion-title>Dr.Profile</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n\n\n<ion-content padding>\n    <ion-card class="cardclass">\n     \n      <span *ngFor="let Ab of drdetailss | async" \n      detail-push >\n      <ion-item><strong>Qualification:</strong>{{Ab.qualification}}</ion-item>\n      \n        <ion-item><strong>Experience:</strong>{{Ab.experience}}</ion-item>\n    \n      <ion-item><strong>Languages:</strong>{{Ab.language}}</ion-item>\n    \n     <br>\n     <br>\n     <ion-row>    \n         <button ion-button block  class="editbtn" round navPush="EditPage" [navParams]="{item:Ab}" >Edit</button>\n        </ion-row>\n        \n        </span>\n    </ion-card>\n\n  \n    </ion-content>\n'/*ion-inline-end:"/home/krishna/krishna/projects/Dr.Application/src/pages/drprofile-details/drprofile-details.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_provider_provider__["a" /* AuthProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], DrprofileDetailsPage);
    return DrprofileDetailsPage;
}());

//# sourceMappingURL=drprofile-details.js.map

/***/ })

});
//# sourceMappingURL=12.js.map