webpackJsonp([3],{

/***/ 602:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SpecialistPageModule", function() { return SpecialistPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__specialist__ = __webpack_require__(621);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SpecialistPageModule = (function () {
    function SpecialistPageModule() {
    }
    SpecialistPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__specialist__["a" /* SpecialistPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__specialist__["a" /* SpecialistPage */]),
            ],
        })
    ], SpecialistPageModule);
    return SpecialistPageModule;
}());

//# sourceMappingURL=specialist.module.js.map

/***/ }),

/***/ 621:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SpecialistPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(182);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the SpecialistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SpecialistPage = (function () {
    function SpecialistPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.user = this.navParams.get("userEmail");
    }
    SpecialistPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SpecialistPage');
    };
    SpecialistPage.prototype.openCardiologyPage = function () {
        this.navCtrl.push('SlotbookingPage', { selectdoctors: 'Cardiologist', 'userEmail': this.user });
    };
    SpecialistPage.prototype.openNephrologyPage = function () {
        this.navCtrl.push('SlotbookingPage', { selectdoctors: 'Nephrologist', 'userEmail': this.user });
    };
    SpecialistPage.prototype.openPulmonologyPage = function () {
        this.navCtrl.push('SlotbookingPage', { selectdoctors: 'Pulmonologist', 'userEmail': this.user });
    };
    SpecialistPage.prototype.openGastroenterologyPage = function () {
        this.navCtrl.push('SlotbookingPage', { selectdoctors: 'Gastroenterologist', 'userEmail': this.user });
    };
    SpecialistPage.prototype.openNeurologyPage = function () {
        this.navCtrl.push('SlotbookingPage', { selectdoctors: 'Neurologist', 'userEmail': this.user });
    };
    SpecialistPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-specialist',template:/*ion-inline-start:"/home/krishna/krishna/projects/Dr.Application/src/pages/specialist/specialist.html"*/'<ion-header>\n    <ion-navbar color="bluegreen">\n       \n          <!-- <img class="grid-image" src="./assets/icon/doct.png"> -->\n          <ion-title>Specialists</ion-title>\n        \n        <!-- <button ion-button menuToggle right>\n            <ion-icon name="menu"></ion-icon>\n          </button> -->\n      </ion-navbar>\n\n \n  </ion-header>\n\n  <!-- Specialists -->\n<ion-content padding class="auth-page">\n\n\n\n    <ion-list (click)="openCardiologyPage()">\n        <ion-item>\n          <ion-thumbnail item-start>\n            <img src="./assets/imgs/heart1.png">\n          </ion-thumbnail>\n          <h2 class="doctorname">Cardiology</h2>\n        </ion-item>\n        \n      </ion-list>\n\n\n      <ion-list (click)="openNephrologyPage()">\n          <ion-item>\n            <ion-thumbnail item-start>\n              <img  src="./assets/imgs/kidny.png">\n            </ion-thumbnail>\n            <h2 class="doctorname">Nephrology</h2>\n          </ion-item>\n          \n        </ion-list>\n        <ion-list (click)="openPulmonologyPage()">\n            <ion-item>\n              <ion-thumbnail item-start>\n                <img  src="./assets/imgs/lungs.png">\n              </ion-thumbnail>\n              <h2 class="doctorname">Pulmonology</h2>\n            </ion-item>\n            \n          </ion-list>\n          <ion-list (click)="openGastroenterologyPage()">\n              <ion-item>\n                <ion-thumbnail item-start>\n                  <img  src="./assets/imgs/gastro.png">\n                </ion-thumbnail>\n                <h2 class="doctorname">Gastroenterology</h2>\n              </ion-item>\n              \n            </ion-list>\n            <ion-list (click)="openNeurologyPage()">\n                <ion-item>\n                  <ion-thumbnail item-start>\n                    <img  src="./assets/imgs/brain.jpg">\n                  </ion-thumbnail>\n                  <h2 class="doctorname">Neurology</h2>\n                </ion-item>\n                \n              </ion-list>\n  <!-- <ion-row >\n    <ion-col col-3>\n      <img src="./assets/imgs/heart.png">\n    </ion-col>\n    <ion-col col-9>\n      <div class="doctorname">Cardiology</div>\n    </ion-col>\n\n  </ion-row> -->\n  <!-- <ion-row >\n    <ion-col col-3>\n      <img style="height: 32px;margin-left: 12px" src="./assets/imgs/kidny.png">\n    </ion-col>\n    <ion-col col-9>\n      <div class="doctorname">Nephrology</div>\n    </ion-col>\n  </ion-row> -->\n  <!-- <ion-row >\n      <ion-col col-3>\n        <img style="height: 31px;margin-left: 12px" src="./assets/imgs/lungs.png">\n      </ion-col>\n      <ion-col col-9>\n        <div class="doctorname">Pulmonology</div>\n      </ion-col>\n    </ion-row> -->\n    <!-- <ion-row >\n        <ion-col col-3>\n          <img style="height: 32px;margin-left: 12px" src="./assets/imgs/gastro.png">\n        </ion-col>\n        <ion-col col-9>\n          <div class="doctorname">Gastroenterology</div>\n        </ion-col>\n      </ion-row> -->\n\n      <!-- <ion-row >\n          <ion-col col-3>\n            <img style="height: 31px;width:30px; margin-left: 12px" src="./assets/imgs/brain.jpg">\n          </ion-col>\n          <ion-col col-9>\n            <div class="doctorname">Neurology</div>\n          </ion-col>\n        </ion-row> -->\n\n</ion-content>'/*ion-inline-end:"/home/krishna/krishna/projects/Dr.Application/src/pages/specialist/specialist.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], SpecialistPage);
    return SpecialistPage;
}());

//# sourceMappingURL=specialist.js.map

/***/ })

});
//# sourceMappingURL=3.js.map